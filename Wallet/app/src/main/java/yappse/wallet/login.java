package yappse.wallet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class login extends Activity {

    public GetData gd = new GetData();
    public RequestResult requestResult = new RequestResult();
    EditText username;
    EditText password;

    SharedPreferences prefs;
    boolean remeberMe;
    ImageView imageViewLogo, imageViewBackground, imageViewEmail, imageViewPassword, imageViewKeepLogin, imageViewLoginCorner;

    ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button buttonLogin = (Button) findViewById(R.id.buttonLogin);
        LinearLayout keepLogin = (LinearLayout) findViewById(R.id.keep_logged_in);
        LinearLayout recover = (LinearLayout) findViewById(R.id.forgot_password);
        username = (EditText) findViewById(R.id.inputUserName);
        password = (EditText) findViewById(R.id.inputPassword);
        spinner = (ProgressBar) findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);
        imageViewLogo = (ImageView)findViewById(R.id.login_logo);
        imageViewBackground = (ImageView)findViewById(R.id.imageViewBackground);
        imageViewEmail = (ImageView)findViewById(R.id.imageViewEmail);
        imageViewPassword = (ImageView)findViewById(R.id.imageViewPassword);
        imageViewKeepLogin = (ImageView)findViewById(R.id.imageViewKeepLogin);
        imageViewLoginCorner = (ImageView)findViewById(R.id.imageViewLoginCorner);

//        Picasso.with(getApplicationContext()).load(R.drawable.login_background).into(imageViewBackground);
//        Picasso.with(getApplicationContext()).load(R.drawable.login_logo).into(imageViewLogo);
        Picasso.with(getApplicationContext()) //Background
                .load(R.drawable.login_background)
                .resize(400, 672)
                .centerCrop()
                .into(imageViewBackground);

        Picasso.with(getApplicationContext()) //LoginLogo
                .load(R.drawable.login_logo)
                .resize(600, 82)
                .centerCrop()
                .into(imageViewLogo);

        Picasso.with(getApplicationContext()) //Username
                .load(R.drawable.email_icon)
                .resize(72, 72)
                .centerCrop()
                .into(imageViewEmail);

        Picasso.with(getApplicationContext()) //Password
                .load(R.drawable.password_icon)
                .resize(72, 72)
                .centerCrop()
                .into(imageViewPassword);

        Picasso.with(getApplicationContext()) //KeepLogin
                .load(R.drawable.keep_login)
                .resize(50, 50)
                .centerCrop()
                .into(imageViewKeepLogin);

        Picasso.with(getApplicationContext()) //LoginCorner
                .load(R.drawable.login_corner_logo)
                .resize(150, 150)
                .centerCrop()
                .into(imageViewLoginCorner);


        //check if credentials saved (keep me logged in)
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isLoggedIn = prefs.getBoolean("isLoggedIn", false);
        if(isLoggedIn)
        {
            String usr = prefs.getString("usr", "");
            String pass = prefs.getString("pass", "");
            String fullname = prefs.getString("fullname", "");
            //new MyTask().execute(usr, pass);
            //if((auth != null) && requestResult.success)
            //{
                //if Authentication is successful, move to the next screen
                //if(auth.Success)
                //{
            try {

                    Intent intent = new Intent(this, MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("fullname", fullname);
                    bundle.putString("userId", usr);
                    bundle.putString("password", pass);
                    intent.putExtras(bundle);
                    startActivity(intent);

            }catch (Exception e)
            {
                e.printStackTrace();
            }
                //}
            //}
        }

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try
                {
                    //dialog = ProgressDialog.show(this, "Loading", "Please wait...", true);
                    new MyTask().execute(username.getText().toString(), password.getText().toString());
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        keepLogin.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                try
                {
                    remeberMe = true;
                    Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

    }

    public void recover(View v)
    {
//        Intent intent = new Intent(this,forgotPassword.class);
//        startActivity(intent);
    }

//    public void remeberMeClick(View v)
//    {
//        remeberMe = true;
//    }

    //ProgressDialog dialog;
    Authentication auth = null;
    public void clickLogin(View v)
    {

        //Authentication auth = gd.authentication(username.getText().toString(), password.getText().toString(), requestResult);

        try {
            //dialog = ProgressDialog.show(this, "Loading", "Please wait...", true);
            new MyTask().execute(username.getText().toString(), password.getText().toString()).get();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }



    public void login()
    {
        if((auth != null) && requestResult.success)
        {
            //if Authentication is successful, move to the next screen
            if(auth.Success)
            {
                Toast.makeText(this, "Authentication successful", Toast.LENGTH_LONG).show();
                if(remeberMe)
                {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("isLoggedIn", true);
                    editor.putString("fullname", auth.FullName);
                    editor.putString("usr", username.getText().toString());
                    editor.putString("pass", password.getText().toString());
                    editor.apply();
                }

                Intent intent = new Intent(this, MainActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("fullname", auth.FullName);
                bundle.putString("userId", auth.UserId);
                bundle.putString("password", password.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(this, "Authentication failed. Wrong username or passowrd", Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(this, "Error while signing in. Please check your connection", Toast.LENGTH_LONG).show();
        }
    }

    private class MyTask extends AsyncTask<String, Void, Void> {

        String pageContent = "";

        @Override
        protected void onPreExecute()
        {
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        public Void doInBackground(String... params) {

            requestResult.setSuccess(false);
            HttpURLConnection connection;
            DataOutputStream wr;
            try {
                //Create connection
                //String url = "http://eazeemart.com/BXAPILive/api/cr/login";
                String url = "http://eazeemart.com/BXAPI/api/cr/login";

                HttpClient client = new DefaultHttpClient();

                JSONObject jsonObj = new JSONObject();
                jsonObj.put("token", "mmvgTfW7aeg8hQElILFBZ5KkTmLPvnOzVxUh9uHdHTBxmBAo6JADhY7MHNPn8cq");
                jsonObj.put("userId", params[0]);
                jsonObj.put("password", params[1]);

                // Create the POST object and add the parameters
                HttpPost httpPost = new HttpPost(url);
                StringEntity entity = new StringEntity(jsonObj.toString(), HTTP.UTF_8);
                entity.setContentType("application/json");
                httpPost.setEntity(entity);

                HttpResponse response = client.execute(httpPost);
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Response Code : " +
                        response.getStatusLine().getStatusCode());

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                System.out.println("Result : " + result.toString());
                pageContent = result.toString();
                System.out.println("Result : " + pageContent);
                requestResult.setSuccess(true);
            } catch (Exception e) {
                e.printStackTrace();
                requestResult.setSuccess(false);
                requestResult.setMessage(e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            //auth = new Gson().fromJson(pageContent, Authentication.class);
            try
            {
//                Gson gson = new Gson();
                JsonParser parser = new JsonParser();
                JsonArray jArray = parser.parse(pageContent).getAsJsonArray();
                String jsonContent = jArray.get(0).toString();
                jsonContent = jsonContent.replace("[", "");
                jsonContent = jsonContent.replace("]", "");
                System.out.println("JSON FILE : " + jsonContent);
                JsonElement jElement = jArray.get(0);

                auth = new Gson().fromJson(jsonContent, Authentication.class);

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            spinner.setVisibility(View.GONE);
            login();
        }
    }
}


