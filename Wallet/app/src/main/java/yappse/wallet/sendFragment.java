package yappse.wallet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

public class sendFragment extends Fragment {

    public GetData gd = new GetData();
    User user;
    Authentication auth;
    public RequestResult requestResult = new RequestResult();
    public String userId, passowrd;

    ImageButton scanQr;
    TextView textViewEUR;
    TextView textViewMCO;
    EditText address;
    EditText amount;
    Button sendButton;

    private ProgressBar spinner;
    ProgressDialog progressDialog;

    private ScrollView scroll;
    Timer t;
    SendMCOResponse sendMCOResponse;

    Intent intent;
    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        intent = activity.getIntent();
        Bundle bundle = intent.getExtras();
        userId = bundle.getString("userId");
        passowrd = bundle.getString("password");
    }

    public sendFragment()
    {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        setRetainInstance(true);
        View view = inflater.inflate(R.layout.fragment_send, container, false);
        scroll = (ScrollView)view.findViewById(R.id.scroll_view_send);

        Activity act = this.getActivity();


        textViewEUR = (TextView) view.findViewById(R.id.textViewEUR);
        textViewMCO = (TextView) view.findViewById(R.id.textViewMCO);
        scanQr = (ImageButton) view.findViewById(R.id.imageButtonScanQR);
        address = (EditText) view.findViewById(R.id.editTextAddress);
        amount = (EditText) view.findViewById(R.id.editTextAmount);
        sendButton = (Button) view.findViewById(R.id.buttonSend);
        spinner = (ProgressBar) view.findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);


        try {
            new MyTaskUser().execute(userId, passowrd);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        scanQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    IntentIntegrator intent = new IntentIntegrator(getActivity());
                    intent.setPrompt("Hold the camera up to the barcode\nAbout 6 inches away\n\nWait for the barcode to automatically scan");
                    intent.initiateScan();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((address.getText()) == null || address.getText().toString() == "")
                {
                    Toast.makeText(getActivity(), "No address", Toast.LENGTH_SHORT).show();
                }
                else if ((amount.getText() == null) || amount.getText().toString() == "")
                {
                    Toast.makeText(getActivity(), "No amount", Toast.LENGTH_SHORT).show();
                }
                else if (address.getText().toString().equals(userId) || address.getText().toString() == userId)
                {
                    Toast.makeText(getActivity(), "You entered your address, please check you input", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if((auth != null) && requestResult.success)
                    {
                        float amountF;

                        if(tryParseFloat(amount.getText().toString()))
                        {
                            amountF = Float.parseFloat(amount.getText().toString());
                            try {
                                new MyTaskSendMCO().execute(userId, address.getText().toString(), amountF+"");
                            } catch (Exception e)
                            {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            Toast.makeText(getActivity(), "Wrong amount value", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

        final CountDownTimer timer;
        timer = new CountDownTimer(300, 300) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                scroll.scrollBy(0, 5000);

            }
        }.start();
        final TimerTask myTask = new TimerTask()
        {
            @Override
            public void run() {
                t.cancel();
                scroll.scrollBy(0, 5000);
            }
        };

        address.setOnFocusChangeListener(new View.OnFocusChangeListener() { // preveri delovanje
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                t = new Timer();
//                t.schedule(myTask, 0);
                timer.start();
                //scroll.scrollTo(0, 3000);
            }
        });

        amount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
//                t = new Timer();
//                t.schedule(myTask, 0);
                timer.start();
               // scroll.scrollTo(0, 3000);
            }
        });

        return view;
    }

    public boolean tryParseFloat(String value)
    {
        try
        {
            Float.parseFloat(value);
            return true;
        }
        catch (NumberFormatException ex)
        {
            return false;
        }
    }

    public void fillTexts()
    {
        if((auth != null) && requestResult.success) {
            DecimalFormat dfC = new DecimalFormat("#.##");
            DecimalFormat dfE = new DecimalFormat("#.###");
            textViewMCO.setText(auth.BalanceCoins + " MCO", TextView.BufferType.NORMAL);
            textViewEUR.setText(auth.BalanceEur + " €", TextView.BufferType.NORMAL);
        }
    }


    public void sendMco()
    {
        if((sendMCOResponse != null) && requestResult.success)
        {
//            if(sendMCOResponse.status.equals("1"))
//            {
//                Toast.makeText(getActivity(), "Transaction processed", Toast.LENGTH_LONG).show();
//            }
//            else if (sendMCOResponse.status.equals("-1"))
//            {
//                Toast.makeText(getActivity(), "Transaction canceled", Toast.LENGTH_LONG).show();
//            }
//            else if (sendMCOResponse.status.equals("0"))
//            {
//                Toast.makeText(getActivity(), "Transaction pending", Toast.LENGTH_LONG).show();
//            }
            address.setText("");
            amount.setText("");
            Toast.makeText(getActivity(), sendMCOResponse.Message, Toast.LENGTH_LONG).show();
            textViewMCO.setText(sendMCOResponse.BalanceCoins + " MCO", TextView.BufferType.NORMAL);
        }
    }

    private class MyTaskUser extends AsyncTask<String, Void, Void> {

        String pageContent = "";

        @Override
        protected void onPreExecute() {
            spinner.setVisibility(View.VISIBLE);
        }


        @Override
        public Void doInBackground(String... params) {

            requestResult.setSuccess(false);
            HttpURLConnection connection;

            try {
                //Create connection
                //String url = "http://eazeemart.com/BXAPILive/api/cr/login";
                String url = "http://eazeemart.com/BXAPI/api/cr/login";

                HttpClient client = new DefaultHttpClient();

                System.out.println("user :  " + params[0] + "  " + params[1]);
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("token", "mmvgTfW7aeg8hQElILFBZ5KkTmLPvnOzVxUh9uHdHTBxmBAo6JADhY7MHNPn8cq");
                jsonObj.put("userId", params[0]);
                jsonObj.put("password", params[1]);

                // Create the POST object and add the parameters
                HttpPost httpPost = new HttpPost(url);
                StringEntity entity = new StringEntity(jsonObj.toString(), HTTP.UTF_8);
                entity.setContentType("application/json");
                httpPost.setEntity(entity);

                HttpResponse response = client.execute(httpPost);
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Response Code : " +
                        response.getStatusLine().getStatusCode());

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                System.out.println("Result : " + result.toString());
                pageContent = result.toString();
                System.out.println("Result : " + pageContent);

                requestResult.setSuccess(true);
            } catch (Exception e) {
                e.printStackTrace();
                requestResult.setSuccess(false);
                requestResult.setMessage(e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(pageContent).getAsJsonArray();
            String jsonContent = jArray.get(0).toString();
            jsonContent = jsonContent.replace("[", "");
            jsonContent = jsonContent.replace("]", "");
            System.out.println("JSON FILE : " + jsonContent);
            JsonElement jElement = jArray.get(0);

            auth = new Gson().fromJson(jsonContent, Authentication.class);
            spinner.setVisibility(View.GONE);
            fillTexts();
        }
    }

    private class MyTaskSendMCO extends AsyncTask<String, Void, Void> {

        String pageContent = "";
        DataOutputStream wr;


        @Override
        protected void onPreExecute() {
            spinner.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "Sending...", Toast.LENGTH_SHORT).show();
        }


        @Override
        public Void doInBackground(String... params) {

            requestResult.setSuccess(false);
            HttpURLConnection connection;

            try {
                //Create connection
//                URL url = new URL("https://mw.coinsrace.com/api/send");
//                connection = (HttpURLConnection) url.openConnection();
//                connection.setRequestMethod("POST");
//                connection.setRequestProperty("Content-Type",
//                        "application/x-www-form-urlencoded");
//
//                String postData = "";
//                String amountStr = params[2].toString();
//
//                postData += URLEncoder.encode("secret") + "=" + URLEncoder.encode(params[0]) + "&";
//                postData += URLEncoder.encode("address") + "=" + URLEncoder.encode(params[1]) + "&";//ko na novo iz nekje pokličem ta activity je secret prazen in zato NulllPointerException
//                postData += URLEncoder.encode("amount") + "=" + URLEncoder.encode(params[2]);
//                byte[] data = postData.getBytes(); // StandardCharsets.US_ASCII
//
//                connection.setRequestProperty("Content-Length", data.length + "");
//                connection.setRequestProperty("Content-Language", "en-US");
//
//                connection.setUseCaches(false);
//                connection.setDoOutput(true);
//
//                //Send request
//                wr = new DataOutputStream(connection.getOutputStream());
//                wr.writeBytes(postData);
//                wr.close();
//
//                //Get Response
//                InputStream is = connection.getInputStream();
//                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
//                StringBuilder response = new StringBuilder(); // or StringBuffer if not Java 5+
//                String s = "";
//                String line;
//                int i = 0;
//                while ((line = rd.readLine()) != null) {
//                    response.append(line);
//                    System.out.println(line);
//                    i++;
//                }
//                //System.out.println(i);
//                rd.close();
//                pageContent = response.toString();

                //String url = "http://eazeemart.com/BXAPILive/api/cr/transferMCO";
                String url = "http://eazeemart.com/BXAPI/api/cr/transferMCO";

                HttpClient client = new DefaultHttpClient();

                JSONObject jsonObj = new JSONObject();
                jsonObj.put("userId", params[0]);
                jsonObj.put("toUserId", params[1]);
                jsonObj.put("MCOQty", params[2]);
                jsonObj.put("token", "mmvgTfW7aeg8hQElILFBZ5KkTmLPvnOzVxUh9uHdHTBxmBAo6JADhY7MHNPn8cq");

                // Create the POST object and add the parameters
                HttpPost httpPost = new HttpPost(url);
                StringEntity entity = new StringEntity(jsonObj.toString(), HTTP.UTF_8);
                entity.setContentType("application/json");
                httpPost.setEntity(entity);

                HttpResponse response = client.execute(httpPost);
                System.out.println("\nSending 'POST' request to URL : " + url);
                System.out.println("Response Code : " +
                        response.getStatusLine().getStatusCode());

                BufferedReader rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                System.out.println("Result : " + result.toString());
                pageContent = result.toString();
                System.out.println("Result : " + pageContent);

                requestResult.setSuccess(true);
            } catch (Exception e) {
                e.printStackTrace();
                requestResult.setSuccess(false);
                requestResult.setMessage(e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            JsonParser parser = new JsonParser();
            JsonArray jArray = parser.parse(pageContent).getAsJsonArray();
            String jsonContent = jArray.get(0).toString();
            jsonContent = jsonContent.replace("[", "");
            jsonContent = jsonContent.replace("]", "");
            System.out.println("JSON FILE : " + jsonContent);
            JsonElement jElement = jArray.get(0);

            sendMCOResponse = new Gson().fromJson(jsonContent, SendMCOResponse.class);
            sendMco();
            spinner.setVisibility(View.GONE);
        }

    }

}
